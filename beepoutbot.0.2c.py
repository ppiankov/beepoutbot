#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Simple telegram bot to delete system messages
This program is NOT dedicated to the public domain under the any license. Use at your own risk.
"""

from __future__ import print_function
import os
import sys
from threading import Thread

import logging
import telegram
from telegram import MessageEntity
from telegram.error import NetworkError, Unauthorized
#from time import sleep
from datetime import datetime
from datetime import timedelta

update_id = None

#replace with real token.
TOKEN = "XXXXXXXXX:ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters,MessageHandler,MessageHandler
import time

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    """Run the bot."""
    global updater
    updater = Updater(TOKEN, use_context=True)
    bot = updater.bot
    dp = updater.dispatcher
    dp.add_handler(MessageHandler(Filters.status_update, vaporize,pass_user_data=True,pass_chat_data=True)) 
    updater.start_polling(timeout=15, read_latency=4)
    updater.idle()

def vaporize(update,context):
    chat_id=update.message.chat_id
    username=update.effective_message.from_user.username
    
    message_id=update.message.message_id
    bot=context.bot
    
    bot.delete_message(chat_id,message_id)


def stop_and_restart():
    """Gracefully stop the Updater and replace the current process with a new one"""
    updater.stop()
    os.execl(sys.executable, sys.executable, *sys.argv)

def restart(update):
#    update.message.reply_text('Bot is restarting...')
    print("restarting")
    Thread(target=stop_and_restart).start()

def error_callback(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


if __name__ == '__main__':
    main()
